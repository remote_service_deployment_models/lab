import os
import psycopg2
from tabulate import tabulate

POSTGRES_URI = os.environ.get('POSTGRES_URI')

def get_exam_dates():
    try:
        conn = psycopg2.connect(POSTGRES_URI)
        cursor = conn.cursor()
        sql = "SELECT exam_date FROM exam;"
        cursor.execute(sql)
        data = cursor.fetchall()
        conn.close()
        return data
    except Exception as e:
        return f"PostgreSQL connection failed. Exception: {e}"

exams = get_exam_dates()

exams_list = [[exam[0]] for exam in exams]

if isinstance(exams, str):
    print(exams)
else:
    headers = ["Exam dates"]
    print(tabulate(exams_list, headers=headers))
