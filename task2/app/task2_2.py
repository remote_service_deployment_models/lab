import os
import psycopg2
import pandas as pd

POSTGRES_URI = os.environ.get('POSTGRES_URI')

def get_exam_dates():
    try:
        conn = psycopg2.connect(POSTGRES_URI)
        sql = "SELECT exam_date FROM exam;"
        data = pd.read_sql_query(sql, conn)
        conn.close()
        return data
    except Exception as e:
        return f"PostgreSQL connection failed. Exception: {e}"

exams = get_exam_dates()
print(f"Exam dates")
print(exams)


# FROM python:3.11

# WORKDIR /app

# RUN pip install psycopg2
# RUN pip install pandas

# COPY . /app/
# CMD ["python", "task2.py"]
