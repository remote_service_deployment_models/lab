CREATE TABLE IF NOT EXISTS student_group (
    group_id serial PRIMARY KEY,
    group_name text
);

CREATE TABLE student (
    stud_id serial PRIMARY KEY,
    group_id integer REFERENCES student_group(group_id),
    stud_first_name text,
    stud_last_name text,
    stud_birthday date
);

CREATE TABLE exam (
    exam_id serial PRIMARY KEY,
    group_id integer REFERENCES student_group(group_id),
    exam_date date,
    exam_subject text,
    exam_teacher_name text
);

INSERT INTO student_group (group_name)
VALUES
    ('БСБО-04-20'),
    ('БСБО-05-20'),
    ('БСБО-06-20'),
    ('БСБО-07-20'),
    ('БСБО-08-20');
    
INSERT INTO student (stud_first_name, stud_last_name, stud_birthday, group_id)
VALUES
    ('Иван', 'Иванов', '2002-05-15', 2),
    ('Петр', 'Петров', '2002-08-20', 2),
    ('Сергей', 'Сергеев', '2002-02-28', 2),
    ('Александр', 'Александров', '2002-10-10', 2),
    ('Елена', 'Еленова', '2002-07-04', 4),
    ('Ольга', 'Ольгова', '2002-12-18', 5);

INSERT INTO exam (group_id, exam_date, exam_subject, exam_teacher_name)
VALUES
    (2, '2023-12-20', 'Математика', 'Петров А.С.'),
    (2, '2023-11-25', 'Физика', 'Иванова Е.В.'),
    (2, '2023-11-25', 'Английский', 'Смирнов К.Д.'),
    (3, '2023-10-15', 'История', 'Павлова О.Н.'),
    (4, '2023-09-30', 'Биология', 'Соколова А.М.');
