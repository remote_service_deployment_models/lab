import os
import random
import string

def generate_password(length=12):
    input_arg = os.environ.get("INPUT_ARG")
    if input_arg and input_arg.isdigit():
        length = int(input_arg)

    characters = string.ascii_letters + string.digits + string.punctuation

    password = ''.join(random.choice(characters) for _ in range(length))
    return password

password = generate_password(random.randint(8, 32))
print("Сгенерированный пароль:", password)
